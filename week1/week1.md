# Week 1
## Project Management


for the first week in FabAcademy we were introduced to Github and Gitlab to manage our websites. The objective of the first week was to:
1. Create a personal website and document our process using Gitlab and other programs used.
2. Describe and sketch our final project and upload it to the website.


#### _Step 1:_
The first step was to create an account on Gitlab and download Git.

#### _Step 2:_
After installing Git and running Terminal on my mac, I linked my GitLab account to my computer using the following commands:

1. git config --global user.name "shadenhesham"
and
2. git config --global user.email "shaden.almutlaq@gmail.com"

_note: before running the user name and email, I used git --version to check that the correct version is installed in my laptop._


#### _Step 3:_
The next step was to generate an SHH key to avoid having to enter my password every time.

After typing the command _ssh-keygen –t –b 4096 –c shaden.almutlaq@gmail.com_ an .ssh folder will generate with a file id_rsa.pud which I copied the text from.

I went to Gitlab > settings > ssh keys and pasted the key and saved it. An email confirmation was received that a new key was added to my account.


#### _Step 4:_
I createed a new project on Gitlab


#### _Step 5:_

To transfer my project from GitLab to my desktop, I cloned my git lab project using the following steps:

1. Copy the SSH link under the clone tab in my project folder: git@gitlab.com:shadenhesham/shaden_website.git
2. Select repository
3. Paste the link to Terminal as follows: git clone git@gitlab.com:shadenhesham/shaden_website.git
4. Type "yes" in terminal to confirm connecting Gitlab to my desktop.


#### _Step 6:_
To link my computer to Gitlab, I first select the local repository then use following commands:

1. git add --all: This command adds all changes included in the next commit.
2. git commit –-m “xxxx”: This command adds a message to each push.
3. Git push: This command pushes all local files to GitLab.

## Creating a website:

#### _Step 1:_
I downloaded the necessary programs needed to begin the work process:
- [Atom](www.atom.io) - Because I don't have much experience with HTML, having Markdown as an option was essential for adding text to my website. I used the [this website](www.markdowntutorial.com) for Markdown tutorials to understand the language and how to use it.
- [Brackets](www.brackets.io) - this tool allows live editing of website HTML code, I used this to add images, delete extra pages from my template, etc. To get a better understandng of HTML, I used [W3schools](www.w3schools.com) which has simple and easy to follow guides.


#### _Step 2:_
For my website, I downloaded a free [template](www.bootstrapmade.com/demo/MyPortfolio/) from [Bootstrap](www.bootsrtapmade.com).

To remove extra pages, change/adjust titles, and remove images from my template I used  **brackets**. I then used **Atom** to write the documentation.


#### _Step 3:_
After adding the content on Atom, I converted the .md file to HTML to add it to my website using brackets. Because Markdown is a different language, you have to convert it to HTML to embed it in my website. I typed the following command on my terminal:

_pandoc week1.md -f markdown -t html -s -o week1.html_


#### _Step 3:_
I then embedded my .HTML file in the index page under the first week's tab, which will add and link the converted .HTML page to my website.

I then plugged in all the images using Brackets, saved my work and used the git push command to update my project on GitLab. To find the website link, I went to my project folder on GitLab > Settings > Pages and found the link to my website.


## Issues I faced:
When configuring my user name and email, my email was not linking properly and I also did not receive a confirmation mail. I requested to received another conformation email but that did not work, so i followed the below steps:

1. My classmate shared this [link](https://about.gitlab.com/handbook/support/workflows/confirmation_emails.html) which has some tips on troubleshooting this issue.
2. I then searched for my email manually to ensure that an account was created using the below link: _https://gitlab.com/api/v4/users?search=email@email.test_
3. I replaced the email at the end with my email: _https://gitlab.com/api/v4/users?search=shaden.almutlaq@gmail.com.test_
3. I hit enter and got a confirmation that the account was created.
4. To double check, I used the following command on Terminal and got a confirmation the account was validated:
